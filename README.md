Rijkswaterstaat-LED-Controller
==============================

©2013 Melvin Vermeeren.  
This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.  
To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.  
Made in Visual Studio 2013 with Visual Micro, CodeMaid and ReSharper. 

Key bindings:
-------------
*	` and ~ switch between input modes.
*	0-9 change animation delays and PWM values depending on input mode.
*	\+ and - change animation delay by a constant amount.
*	Available animations: H, h, S, s, R, L and E to turn everything off.
*	Lower case animations are upper case animations but inverted LED states.

Button bindings:
----------------
*	Left button reduces LED brightness.
*	Middle button increases LED brightness.
*	Right button switches between animations.