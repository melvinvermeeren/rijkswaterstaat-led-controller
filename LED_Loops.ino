/**
 * �2013 Melvin Vermeeren.
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
 */


/**
 * LED loop 'H'.
 */
void LEDLoop_H()
{
	if (!inSecondAnimation) // Left to right.
	{
		if (animationPosition > 0) isLedIDOn[animationPosition - 1] = false;
		isLedIDOn[animationPosition] = true;
	}

	else // Right to left.
	{
		if (animationPosition < LEDPINIDS_LENGTH - 1) isLedIDOn[animationPosition + 1] = false;
		isLedIDOn[animationPosition] = true;
	}
}


/**
 * LED loop 'h'.
 */
void LEDLoop_h()
{
	if (!inSecondAnimation) // Left to right.
	{
		if (animationPosition > 0) isLedIDOn[animationPosition - 1] = true;
		isLedIDOn[animationPosition] = false;
	}

	else // Right to left.
	{
		if (animationPosition < LEDPINIDS_LENGTH - 1) isLedIDOn[animationPosition + 1] = true;
		isLedIDOn[animationPosition] = false;
	}
}


/**
 * LED loop 'S'.
 */
void LEDLoop_S()
{
	if (animationPosition > 0)
	{
		isLedIDOn[animationPosition - 1] = false;
		isLedIDOn[LEDPINIDS_LENGTH - animationPosition] = false;
	}
	isLedIDOn[animationPosition] = true;
	isLedIDOn[LEDPINIDS_LENGTH - 1 - animationPosition] = true;
}


/**
 * LED loop 's'.
 */
void LEDLoop_s()
{
	if (animationPosition > 0)
	{
		isLedIDOn[animationPosition - 1] = true;
		isLedIDOn[LEDPINIDS_LENGTH - animationPosition] = true;
	}
	isLedIDOn[animationPosition] = false;
	isLedIDOn[LEDPINIDS_LENGTH - 1 - animationPosition] = false;
}


/**
 * Led loop 'R'.
 */
void LEDLoop_R()
{
	if (animationPosition == LEDPINIDS_LENGTH) turnAllLEDs(false);
	else isLedIDOn[animationPosition] = true;
}


/**
 * Led loop 'L'.
 */
void LEDLoop_L()
{
	if (animationPosition == LEDPINIDS_LENGTH) turnAllLEDs(false);
	else isLedIDOn[LEDPINIDS_LENGTH - 1 - animationPosition] = true;
}