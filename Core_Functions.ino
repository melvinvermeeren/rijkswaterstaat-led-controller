/**
 * �2013 Melvin Vermeeren.
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
 */


/**
 * Process serial input.
 * Also sets all LEDs to on or off based on what's needed for the new animation type.
 */
void processSerialInput()
{
	if (Serial.available() > 0)
	{
		/* When input is longer than 1 character, give controller some time to catch it all.
		   Apparently it does 1 character every 1-2 milliseconds, so even more delay is added in the while loop to make sure only last character is loaded. */
		delay(2);

		while (Serial.available() > 1)
		{
			Serial.read(); // Get rid of everything except for the latest input.
			delay(2); // Delay to allow controller to catch more input before checking again.
		}

		char incomingByte = Serial.read(); // Put latest input in memory.

		/* Process delayAnimation input, check for '0' till '9','+' and '-'. */
		if ((isInputDelayAnimation && incomingByte >= '0' && incomingByte <= '9') || incomingByte == '+' || incomingByte == '-')
		{
			bool overrideMessage = false; // Is user reaches constraints override default message.
			switch (incomingByte)
			{
			case '0':
				animationDelay = 50;
				break;

			case '1':
				animationDelay = 100;
				break;

			case '2':
				animationDelay = 150;
				break;

			case '3':
				animationDelay = 200;
				break;

			case '4':
				animationDelay = 250;
				break;

			case '5':
				animationDelay = 300;
				break;

			case '6':
				animationDelay = 350;
				break;

			case '7':
				animationDelay = 400;
				break;

			case '8':
				animationDelay = 450;
				break;

			case '9':
				animationDelay = 500;
				break;

			case '+':
				/* First part of the first "if" statement makes sure that it doesn't space out when it's near the limit of the data type. */
				if (animationDelay <= MAXIMUM_ANIMATIONDELAY - ANIMATIONDELAY_JUMPBY && animationDelay + ANIMATIONDELAY_JUMPBY <= MAXIMUM_ANIMATIONDELAY) animationDelay += ANIMATIONDELAY_JUMPBY; // Try to add value first.
				else if (animationDelay != MAXIMUM_ANIMATIONDELAY) animationDelay = MAXIMUM_ANIMATIONDELAY; // If it goes over the maximum set animationDelay to maximum value.
				else overrideMessage = true; // Already maximum so display message.
				break;

			case '-':
				/* First part of the first "if" statement makes sure that it doesn't space out when it's near the limit of the data type. */
				if (animationDelay >= ANIMATIONDELAY_JUMPBY && animationDelay - ANIMATIONDELAY_JUMPBY >= MINIMUM_ANIMATIONDELAY) animationDelay -= ANIMATIONDELAY_JUMPBY; // Try to subtract value first.
				else if (animationDelay != MINIMUM_ANIMATIONDELAY) animationDelay = MINIMUM_ANIMATIONDELAY; // If it goes over the minimum set animationDelay to minimum value.
				else overrideMessage = true; // Already minimum so display message.
				break;
			}
			if (overrideMessage) Serial.print("Animation delay limit reached: ");
			else Serial.print("Animation delay set: ");
			Serial.print(animationDelay);
			Serial.println(" ms.");
			if (overrideMessage) Serial.println("Try a lower or higher value instead.");
		}

		/* Process PWM input, check for '0' till '9'. */
		else if (isPWMEnabled && incomingByte >= '0' && incomingByte <= '9') updatePWMVariables(incomingByte - 48); // Minus 48 to get integer value instead of ASCII ID.

		/* Not this either? Check if it's a input toggle key. */
		else if (isPWMEnabled && (incomingByte == inputToggleChar1 || incomingByte == inputToggleChar2))
		{
			Serial.print("Input mode is now: ");
			if (isInputDelayAnimation)
			{
				isInputDelayAnimation = false;
				Serial.println("PWM control.");
			}
			else
			{
				isInputDelayAnimation = true;
				Serial.println("Animation delay control.");
			}
		}

		/* None of those? It has to be a status ID. calculateNewStates() handles statusID errors. */
		else if (statusID != incomingByte) {
			statusID = incomingByte;
			newAnimationResetVariables();
		}

		/* If it's not a new statusID either then the input is the current statusID. */
		else
		{
			Serial.print("Status ID is already ");
			Serial.print(statusID);
			Serial.println(".");
		}
	}
}


/**
 * Set new PWM status.
 * @param new PWMstatusID, 0 through 9.
 */
void updatePWMVariables(unsigned int inputVar)
{
	int PWMPercentage; // Is not actually used to calculate with, is ONLY used to display the correct message.

	switch (inputVar)
	{
		/**
		 * PWM is the correct term for controlling things like LEDs WITH duty cycles.
		 * Check out https://en.wikipedia.org/wiki/Pulse-width_modulation for more information.
		 *
		 * How this works:
		 *
		 * Let's say you want a LED to be on 20% of the time.
		 * That would mean:
		 * Reset when 100% has been reached: PWMResetWhenCounterIsAt = 100;
		 * LED must be on 20% of the time, so: PWMHIGHSignalWhenCounterIsLowerThan = 20;
		 * 20/100 = 20% of the time it's turned on.
		 *
		 * However, you should always simplify fractions! Just keep dividing by 2 manually.
		 * So 20/100 ==> 10/50 ==> 5/25 ==> 1/5;
		 *
		 * So, in the end:
		 * PWMResetWhenCounterIsAt = 5;
		 * PWMHIGHSignalWhenCounterIsLowerThan = 1;
		 *
		 * These are the settings you need for a 20% PWM cycle!
		 *
		 * Note that PWMPercentage is ONLY used to display the correct message. It is NEVER used for any kind of calculation!
		 * PWMCounter is always set to 0 since it's a just a counter.
		 *
		 * PWMResetWhenCounterIsAt's reset is done before LED states are changed so that value is the amount of slots in the counter.
		 */

	case 0: // 1% PWM.
		PWMPercentage = 1;
		PWMResetWhenCounterIsAt = 100;
		PWMHIGHSignalWhenCounterIsLowerThan = 1;
		break;

	case 1: // 2% PWM.
		PWMPercentage = 2;
		PWMResetWhenCounterIsAt = 50;
		PWMHIGHSignalWhenCounterIsLowerThan = 1;
		break;

	case 2: // 5% PWM.
		PWMPercentage = 5;
		PWMResetWhenCounterIsAt = 20;
		PWMHIGHSignalWhenCounterIsLowerThan = 1;
		break;

	case 3: // 10% PWM.
		PWMPercentage = 10;
		PWMResetWhenCounterIsAt = 10;
		PWMHIGHSignalWhenCounterIsLowerThan = 1;
		break;

	case 4: // 20% PWM.
		PWMPercentage = 20;
		PWMResetWhenCounterIsAt = 5;
		PWMHIGHSignalWhenCounterIsLowerThan = 1;
		break;

	case 5: // 30% PWM.
		PWMPercentage = 30;
		PWMResetWhenCounterIsAt = 50;
		PWMHIGHSignalWhenCounterIsLowerThan = 15;
		break;

	case 6: // 40% PWM.
		PWMPercentage = 40;
		PWMResetWhenCounterIsAt = 25;
		PWMHIGHSignalWhenCounterIsLowerThan = 10;
		break;

	case 7: // 60% PWM.
		PWMPercentage = 60;
		PWMResetWhenCounterIsAt = 25;
		PWMHIGHSignalWhenCounterIsLowerThan = 15;
		break;

	case 8: // 80% PWM.
		PWMPercentage = 80;
		PWMResetWhenCounterIsAt = 5;
		PWMHIGHSignalWhenCounterIsLowerThan = 4;
		break;

	default: // 100% PWM.
		PWMPercentage = 100;
		PWMResetWhenCounterIsAt = 1;
		PWMHIGHSignalWhenCounterIsLowerThan = 1;
		break;
	}
	PWMstatusID = inputVar;
	PWMCounter = 0; // Reset the PWM counter.

	if (isSerialEnabled)
	{
		Serial.print("PWM percentage set: ");
		Serial.print(PWMPercentage);
		Serial.println("%.");
	}
}


/**
 * Process button input.
 */
void processButtonInput()
{
	/* Process button 0: StatusID switcher. */
	if (millis() > buttonTimer)
	{
		/* PWM + button has priority, so check for it first. */
		if (isPWMEnabled && digitalRead(BUTTONIDS[1]) == BUTTONIDS_REACTWHENSTATEIS[1])
		{
			/* Set button timer to 0 when it's near the limit of millis(). */
			if (millis() > ULONG_MAX_MARGIN - BLOCKBUTTONSFOR) buttonTimer = 0;
			else buttonTimer = millis() + BLOCKBUTTONSFOR;

			if (PWMstatusID < 9)	updatePWMVariables(PWMstatusID + 1);
		}

		/* Else check for PWM - button. */
		else if (isPWMEnabled && digitalRead(BUTTONIDS[0]) == BUTTONIDS_REACTWHENSTATEIS[0])
		{
			/* Set button timer to 0 when it's near the limit of millis(). */
			if (millis() > ULONG_MAX_MARGIN - BLOCKBUTTONSFOR) buttonTimer = BLOCKBUTTONSFOR;
			else buttonTimer = millis() + BLOCKBUTTONSFOR;

			if (PWMstatusID > 0)	updatePWMVariables(PWMstatusID - 1);
		}

		/* Always check the statusID switcher button. */
		if (digitalRead(BUTTONIDS[2]) == BUTTONIDS_REACTWHENSTATEIS[2])
		{
			/* Set button timer to 0 when it's near the limit of millis(). */
			if (millis() > ULONG_MAX_MARGIN - BLOCKBUTTONSFOR) buttonTimer = BLOCKBUTTONSFOR;
			else buttonTimer = millis() + BLOCKBUTTONSFOR;

			switch (statusID)
			{
			case 'H':
				statusID = 'h';
				break;

			case 'h':
				statusID = 'S';
				break;

			case 'S':
				statusID = 's';
				break;

			case 's':
				statusID = 'R';
				break;

			case 'R':
				statusID = 'L';
				break;

			case 'L':
				statusID = 'E';
				break;

			case 'E':
				statusID = 'H';
				break;

			default:
				/* Invalid statusID, break and let calculateNewStates() fix it. Also check for limit of millis(), see above. */
				if (millis() > BLOCKBUTTONSFOR) buttonTimer = millis() - BLOCKBUTTONSFOR; // Do not wait before checking button input again.
				else buttonTimer = 0;
				break;
			}
			newAnimationResetVariables();
		}
	}
}


/**
 * Switch all LEDs on or off.
 * @param true or false.
 */
void turnAllLEDs(bool newStatus)
{
	for (unsigned int i = 0; i < LEDPINIDS_LENGTH; i++)
	{
		isLedIDOn[i] = newStatus;
	}
}


/**
 * Reset animation variables.
 * Also prints a message about the new statusID.
 */
void newAnimationResetVariables()
{
	if (isSerialEnabled)
	{
		Serial.print("Status ID set: ");
		Serial.print(statusID);
		Serial.println(".");
	}

	if (statusID == 'h' || statusID == 's') turnAllLEDs(true); // Turn all LEDs on for statusIDs 'h' and 's'.
	else turnAllLEDs(false);
	animationPosition = 0;
	inSecondAnimation = false;
	if (millis() > animationDelay) lastTime = millis() - animationDelay; // Do not wait before starting animation. Make sure it never goes negative to prevent crashes.
	else lastTime = animationDelay; // If lastTime would go negative due to data type limits set lastTime to animationDelay.
}


/**
 * Calculate new LED states.
 * This method is basically a switch that redirects everything to the correct loops.
 * It also handles the delays since they are universal right now.
 */
void calculateNewStates()
{
	if (millis() > lastTime)
	{
		/* After 4294967295 milliseconds (~50 days) (the limit of unsigned long) millis() will go back to 0.
		   Anticipate this and set lastTime to 0 to prevent freezing of the program after ~50 days.
		   A margin is removed (see top of this file) to make sure it'll always happen in time.
		   This will probably mess up the LED timing once when it actually happens. */
		if (millis() > ULONG_MAX_MARGIN - animationDelay) lastTime = animationDelay;
		else lastTime = millis() + animationDelay;

		switch (statusID)
		{
		case 'E':
			// 'E' is nothing really, so just break.
			break;

		case 'H':
			if (animationPosition == LEDPINIDS_LENGTH && !inSecondAnimation)
			{
				inSecondAnimation = true;
				animationPosition -= 2;
			}
			else if (animationPosition == 0 && inSecondAnimation)
			{
				inSecondAnimation = false;
				isLedIDOn[1] = false; // Manual override to turn off the LED at right side.
			}
			LEDLoop_H();
			break;

		case 'h':
			if (animationPosition == LEDPINIDS_LENGTH && !inSecondAnimation)
			{
				inSecondAnimation = true;
				animationPosition -= 2;
			}
			else if (animationPosition == 0 && inSecondAnimation)
			{
				inSecondAnimation = false;
				isLedIDOn[1] = true; // Manual override to turn on the LED at right side.
			}
			LEDLoop_h();
			break;

		case 'S':
			if (animationPosition == int(LEDPINIDS_LENGTH / 2))
			{
				animationPosition = 0;
				isLedIDOn[int(LEDPINIDS_LENGTH / 2)] = false; // Manual override to turn off centre LEDs.
				isLedIDOn[int(LEDPINIDS_LENGTH / 2 - 1)] = false; // See above.
			}
			LEDLoop_S();
			break;

		case 's':
			if (animationPosition == int(LEDPINIDS_LENGTH / 2))
			{
				animationPosition = 0;
				isLedIDOn[int(LEDPINIDS_LENGTH / 2)] = true; // Manual override to turn on centre LEDs.
				isLedIDOn[int(LEDPINIDS_LENGTH / 2 - 1)] = true; // See above.
			}
			LEDLoop_s();
			break;

		case 'R':
			if (animationPosition == LEDPINIDS_LENGTH + 1) // +1 for the one state 0 LEDs are on, see LEDLoop_R.
			{
				animationPosition = 0;
			}
			LEDLoop_R();
			break;

		case 'L':
			if (animationPosition == LEDPINIDS_LENGTH + 1) // +1 for the one state 0 LEDs are on, see LEDLoop_L.
			{
				animationPosition = 0;
			}
			LEDLoop_L();
			break;

		default: // Invalid input.
			if (isSerialEnabled) Serial.println("Invalid status ID, status ID set to E.");
			statusID = 'E';
			break;
		}
		if (!inSecondAnimation) animationPosition++;
		else animationPosition--;
	}
}


/**
 * Update LED states.
 */
void updateLEDStates()
{
	/* Reset PWMCounter if needed. */
	if (isPWMEnabled && PWMCounter == PWMResetWhenCounterIsAt) PWMCounter = 0;

	/* Set LED states. */
	int newState;

	for (unsigned int i = 0; i < LEDPINIDS_LENGTH; i++)
	{
		/* This is written "double" like this to allow the compiler to optimize it properly. */
		if (isPWMEnabled)
		{
			if (PWMCounter < PWMHIGHSignalWhenCounterIsLowerThan && isLedIDOn[i]) newState = HIGH;
			else newState = LOW;
		}
		else if (!isPWMEnabled)
		{
			if (isLedIDOn[i]) newState = HIGH;
			else newState = LOW;
		}
		digitalWrite(LEDPINIDS[i], newState);
	}
	/* Update PWM Counter. */
	if (isPWMEnabled) PWMCounter++;
}