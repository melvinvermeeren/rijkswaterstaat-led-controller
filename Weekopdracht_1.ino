/**
 * �2013 Melvin Vermeeren.
 * Application: Rijkswaterstaat LED Controller.
 * Initial release 15-Nov-2013.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
 *
 * Key bindings:
 * ` and ~ switch between input modes.
 * 0-9 change animation delays and PWM values depending on input mode.
 * + and - change animation delay by a constant amount.
 * Available animations: H, h, S, s, R, L and E to turn everything off.
 * Lower case animations are upper case animations but inverted LED states.
 *
 * Button bindings:
 * Left button reduces LED brightness.
 * Middle button increases LED brightness.
 * Right button switches between animations.
 * Made in Visual Studio 2013 with Visual Micro, CodeMaid and ReSharper.
 */


/* Configure modules, disabling modules reduces program footprint and increasing loops per second.
   If there is no valid input type enabled an animated error message will be looped. */
const bool isPWMEnabled = true; // Put to false to disable PWM.
const bool isSerialEnabled = true; // Put to false to disable Serial in- and output.
const bool isButtonInputEnabled = true; // Put to false to disable all button input.

/* Configure constants based off includes first. */
#include <limits.h> // Include limits.h since it provides constants of maximum value of variables. ULONG_MAX is used to prevent program freezing after ~50 days.
const unsigned long ULONG_MAX_MARGIN = ULONG_MAX - 50; // ULONG_MAX with margin removed to make sure that program won't freeze.

/* Variables for debugging. */
const bool debugCyclesEnable = false; // Display message every second saying how many times it went through loop().
unsigned long loopsPerSecond = 0; // Counter for amount of loops per second.
unsigned long secTimerLPS; // One-second timer for keeping track of loopsPerSecond properly.

/* Key bindings for toggling between animationDelay input and PWM input. */
const char inputToggleChar1 = '`';
const char inputToggleChar2 = '~';

/* Constants. */
const unsigned int LEDPINIDS[] = { 6, 7, 8, 9, 10, 11, 12, 13 }; // From left to right pin IDs.
const unsigned int LEDPINIDS_LENGTH = 8; // Length of the LEDPINIDS array. Animation 'S' and 's' do not work properly with UNEVEN numbers. Minimum value must be 4, it scales with larger values.
const unsigned int BLOCKBUTTONSFOR = 250; // Amount of milliseconds to ignore button input after a button is pressed.
const unsigned int MINIMUM_ANIMATIONDELAY = 10; // Minimum animationDelay value.
const unsigned int MAXIMUM_ANIMATIONDELAY = 2500; // Maximum animationDelay value.
const unsigned int ANIMATIONDELAY_JUMPBY = 25; // How many ms to add or subtract from animationDelay when user presses '+' or '-'.
/* The following 2 constants are signed to prevent signed and unsigned integer comparison. */
const int BUTTONIDS[] = { 2, 3, 4 }; // From left to right buttons in these pin IDs: PWM -, PWM +, statusID switcher.
const int BUTTONIDS_REACTWHENSTATEIS[] = { HIGH, HIGH, HIGH }; // React to button presses if the state is this, same order as BUTTONIDS[].

/* Global variables. */
unsigned int animationDelay = 250; // Delay in ms between animation state changes.
unsigned int animationPosition = 0; // Current position in animation.
unsigned long lastTime; // Timer for animations.
unsigned long buttonTimer; // Timer for blocking button input.
bool inSecondAnimation = false; // Is the animation in the second state?
bool isLedIDOn[LEDPINIDS_LENGTH]; // Is LED on PINID on?
bool isInputDelayAnimation = true; // If false input is for PWM controls.

/* PWM variables. Check out updatePWMVariables() to modify settings. */
unsigned int PWMstatusID = 9; // Current PWM statusID, 0 is min PWM, 9 is max PWM.
unsigned int PWMCounter = 0; // Is used to decide when it's time to set a LED to HIGH.
unsigned int PWMResetWhenCounterIsAt = 1; // If this number is reached reset it to 0, done before setting LED states.
unsigned int PWMHIGHSignalWhenCounterIsLowerThan = 1; // Give LED HIGH signal if PWMCounter < this setting's value.

/**
 * Status IDs (copied from assignment):
 * H (72):  Heen en weer, waarbij telkens maar ��n lamp brandt.
 * h (104): Heen en weer, waarbij telkens maar ��n lamp uitgeschakeld is, de rest brandt
 * S (83):  Samenvoegen, de buitenste 2 lampen branden en vervolgens branden lampen 2 en 7, daarna 3 en 6, als laatste 4 en 5
 * s (115): Samenvoegen, de buitenste 2 lampen branden; dan gaan lampen 2 en 7 ook branden, dan 3 en 6 ook, als laatste gaan 4 en 5 aan. Er branden dus steeds gelijktijdig 6 lampen.
 * R (82):  Gesloten strook naar rechts, eerst gaat LED 1 branden, dan ook 2, dan ook 3 etc.
 * L (76):  Gesloten strook naar links, eerst gaat LED 8 branden, dan ook 7, dan ook 6 etc.
 * E (69):  Stoppen.
 */
char statusID = 'E';


void setup()
{
	/* If no valid input mode is selected keep printing a message. */
	if (!isSerialEnabled && !isButtonInputEnabled)
	{
		const unsigned int lengthOfMainMessage = 31;
		Serial.begin(9600);
		String message = "Error: No input module enabled";
		char messageArray[lengthOfMainMessage];
		message.toCharArray(messageArray, lengthOfMainMessage);
		delay(100); // Wait for Serial port to start up.

		/* Keep application in an infinite loop and keep printing the message. */
		while (Serial.available() >= 0) // This is always true, but the compiler isn't stupid so it needs to use an actual function.
		{
			while (Serial.available() > 0) Serial.read(); // Prevent user from overflowing serial buffer.

			/* Print main message. */
			for (unsigned int i = 0; i < lengthOfMainMessage; i++)
			{
				Serial.print(messageArray[i]);
				delay(150);
			}

			/* Add 4 periods. */
			for (unsigned int i = 0; i < 3; i++)
			{
				Serial.print(".");
				delay(1000);
			}
			delay(2000);
			Serial.println("");
			/* Print --- line between each message. */
			for (unsigned int i = 0; i < lengthOfMainMessage + 2; i++)
			{
				Serial.print("-");
				delay(100);
			}
			delay(900);
			Serial.println("");
		}
	}

	/* At least one input module on? Run the application. */
	if (isSerialEnabled) Serial.begin(9600); // Open serial port.

	for (unsigned int i = 0; i < LEDPINIDS_LENGTH; i++)
	{
		pinMode(LEDPINIDS[i], OUTPUT); // Assign LEDs to pins.
	}
	turnAllLEDs(false); // Turn all of the LEDs off.

	/* Configure buttons. */
	if (isButtonInputEnabled && isPWMEnabled) pinMode(BUTTONIDS[0], INPUT);
	if (isButtonInputEnabled && isPWMEnabled) pinMode(BUTTONIDS[1], INPUT);
	if (isButtonInputEnabled) pinMode(BUTTONIDS[2], INPUT);

	/* Configure timers. */
	if (isButtonInputEnabled) buttonTimer = millis();
	if (debugCyclesEnable) secTimerLPS = millis();
	lastTime = millis();
}


void loop()
{
	/* If cycles counter is enabled, print the amount of cycles to the console after one second. */
	if (debugCyclesEnable && millis() > secTimerLPS)
	{
		/* Set timer to 0 if it's near the limit of millis(). */
		if (millis() > ULONG_MAX_MARGIN - 1000) secTimerLPS = 1000;
		else secTimerLPS = millis() + 1000;
		Serial.println(loopsPerSecond);
		loopsPerSecond = 0;
	}

	if (isSerialEnabled) processSerialInput(); // Process serial input.

	if (isButtonInputEnabled) processButtonInput(); // Process button input.

	calculateNewStates(); // Calculate new states.

	updateLEDStates(); // Set LED states.

	if (debugCyclesEnable) loopsPerSecond++; // Update cycle counter if it's enabled.
}